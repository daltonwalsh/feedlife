   
    var mainTl = new TimelineLite();
    var dur = 1;
    var defaultEase = Power1.easeInOut;

    mainTl.to("#caraousel-container", dur, {xPercent: 0,ease: defaultEase}, "+=" + dur * 0.5);

    mainTl.to("#caraousel-container", dur, {xPercent: -12,ease: defaultEase}, "+=" + dur * 0.5);

    mainTl.to("#caraousel-container", dur, {xPercent: -24,ease: defaultEase}, "+=" + dur * 0.5);

    mainTl.to("#caraousel-container", dur, {xPercent: -36,ease: defaultEase}, "+=" + dur * 0.5);